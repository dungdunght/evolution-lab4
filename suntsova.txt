{
    "_id": "",
    "index": ,
    "guid": "4c5ce73d-acd4-45fd-bdd7-653225e91d9b",
    "isActive": true,
    "balance": "$3,297.90",
    "picture": "",
    "age": 40,
    "eyeColor": "",
    "name": "Cox Rhodes",
    "gender": "male",
    "company": "ECLIPSENT",
    "email": "cm",
    "phone": "+1 (878) 549-3102",
    "address": "167 Havemeyer Street, Allentown, New York, 7875",
    "about": "Est aute do qui esse nulla nisi incididunt eiusmod. Proident ullamco reprehenderit sunt proident deserunt ex cillum velit nisi cillum quis et pariatur commodo. Aute dolore exercitation velit esse ipsum consequat sit nostrud commodo adipisicing. Proident est deserunt ad duis duis Lorem est sit ullamco. Ex nisi et eu et incididunt. Anim culpa duis sit exercitation et adipisicing. Fugiat veniam sint nostrud nulla eiusmod irure eiusmod.\r\n",
    "registered": "2015-10-26T01:08:28 -03:00",
    "latitude": 48.25115,
    "longitude": -85.242818,
    "tags": [
      "in",
      "tempor",
      "minim",
      "nisi",
      "incididunt",
      "mollit",
      "exercitation"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Justice Conner"
      },
      {
        "id": 1,
        "name": "Annette Dunn"
      },
      {
        "id": 2,
        "name": "Wade Horne"
      }
    ],
    "greeting": "Hello, Cox Rhodes! You have 1 unread messages.",
    "favoriteFruit": "strawberry"
  }